# Practico 5 - Rotación + Traslacion

[Rotacion_Traslacion.py](https://gitlab.com/vision-por-computadora615916/practico-5-rotacion-traslacion/-/blob/main/Rotacion_Traslacion.py?ref_type=heads) Este script en Python utiliza OpenCV para permitir al usuario dibujar un rectángulo sobre una imagen con el ratón, recortar la sección seleccionada y aplicar operaciones de traslación y rotación, guardando los resultados en nuevos archivos.

## Funcionalidad

- Configuración Inicial: Importa las bibliotecas necesarias y define variables globales para el ángulo de rotación, desplazamiento y escala.

- Definición de Callback: Define la función draw_circle que maneja los eventos del ratón para iniciar, finalizar y dibujar el rectángulo, además de recortar la imagen seleccionada.

- Funciones de Transformación: Define funciones para trasladar (translate) y rotar (rotate) la imagen.

- Carga de Imagen: Carga una imagen llamada `hoja.png` para el procesamiento.

- Bucle Principal: En un bucle, muestra la imagen y escucha las teclas:

        q: Salir del programa.

        g: Guardar el recorte del rectángulo en recorte.png.

        e: Aplicar traslación y rotación, y guardar el resultado en `traslado_rotacion.png`.
        
        r: Reiniciar la imagen para empezar de nuevo.